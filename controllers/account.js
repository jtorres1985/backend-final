'use strict'

var requestJson = require('request-json');
const config=require('../config');
const url=config.mlab_host+config.mlab_db+'collections/';


//GET ALL ACCOUNTS
function getAccounts(request,response){
    console.log("Get todas las cuentas");
    var client = requestJson.createClient(url);
    const queryName='q={"cliente":'+request.params.id+'}&';
    const filter='f={"_id":0}&';
    client.get(config.mlab_collection_account+'?'+queryName + filter +config.mlab_key, function(err, res, body) {
      console.log(body);
      response.send(body);
    });
}

module.exports={
  getAccounts
};
