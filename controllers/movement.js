'use strict'

var requestJson = require('request-json');
const config=require('../config');
const url=config.mlab_host+config.mlab_db+'collections/';

//GET MOVIMIENTOS POR CUENTA
function getMovements(request,response){
    console.log("Get Movimientos");
    var client = requestJson.createClient(url);
    const queryName='q={"idCuenta":'+request.params.id+'}&';
    const filter='f={"movimiento":1,"_id":0}&';
    console.log(config.mlab_collection_account+'?'+queryName + filter +config.mlab_key);
    client.get(config.mlab_collection_account+'?'+queryName + filter +config.mlab_key, function(err, res, body) {
      console.log(body[0]);
      response.send(body[0]);
    });
}

//PUT MOVIMIENTO
function saveMovement(request,response){
      console.log("Grabar movimiento");
      //VALIDATE INPUT
      request.checkBody("importe", "Importe esta vacio").isDecimal();
      request.checkBody("tipo", "Tipo esta vacio").notEmpty();

      var client = requestJson.createClient(url);
      const queryName='q={"idCuenta":'+request.params.id+'}&';
      client.get(config.mlab_collection_account+'?'+queryName +config.mlab_key, function(err, res, body) {
        let respon = body[0];
        let newID = body[0].movimiento.length + 1;
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth()+1;
        let year = date.getFullYear();
        let fechaActual =day+"/"+month+"/"+year;
        let newMov = {
          "id" : newID,
          "fecha" : fechaActual,
          "importe" : request.body.importe,
          "tipo" : request.body.tipo,
        };
        let cuenta = body[0];
        cuenta.movimiento.push(newMov);
        client.put(config.mlab_collection_account+'/' +respon._id.$oid + '?'+config.mlab_key, cuenta, function(err, res, body) {
        if(err)console.log(err);
        response.send(body);
        });
      });
};

module.exports={
  getMovements,
  saveMovement
};
