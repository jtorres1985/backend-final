'use strict'

const express=require('express');
const userController=require('../controllers/user');
const authController=require('../controllers/auth');
const accountController=require('../controllers/account');
const movementController=require('../controllers/movement');
const seg=require('../middlewares');
const api = express.Router();

//USERS
//api.get('/users',userController.getUsers);
api.get('/users',seg.isAuth,userController.getUsers);
api.get('/users/:id',seg.isAuth,userController.getUser);
api.post('/users',seg.isAuth,userController.saveUser);
api.put('/users/:id',seg.isAuth,userController.updateUser);
api.delete('/users/:id',seg.isAuth,userController.removeUser);
//LOGIN
api.post('/login',authController.login);
api.post('/logout',authController.logout);
//ACCOUNTS
api.get('/accounts/:id',seg.isAuth,accountController.getAccounts);
//MOVEMENTS
api.get('/movements/:id',seg.isAuth,movementController.getMovements);
api.put('/movements/:id',seg.isAuth,movementController.saveMovement);

//TOKEN
api.get('/seg',seg.isAuth,function (req,res){
  res.status(200).send({message:'ACCESO OK'});
});
api.get('/',function (req,res){
  res.status(200).send({message:'BIENVENIDOS A NUESTRA API'});
});

module.exports=api;
